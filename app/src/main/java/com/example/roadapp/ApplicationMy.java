package com.example.roadapp;

import android.app.Application;

import com.example.libaccelerometerdata.AccelerometerData;

import java.util.ArrayList;

public class ApplicationMy extends Application {
    public ArrayList<AccelerometerData> accelerometerData;

    @Override
    public void onCreate() {
        super.onCreate();
        accelerometerData = new ArrayList<AccelerometerData>();
    }

    public void addAccelerometerData(AccelerometerData a){
        accelerometerData.add(a);
    }

    public void clearAccelerometerData(){
        accelerometerData.clear();
    }

    ArrayList<AccelerometerData> getAccelerometerData(){
        return accelerometerData;
    }
}
