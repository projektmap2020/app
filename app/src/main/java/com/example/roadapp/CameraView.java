package com.example.roadapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;

import com.example.libaccelerometerdata.AccelerometerData;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class CameraView extends Activity implements SensorEventListener, LocationListener {
    private SensorManager sensorManager;
    private LocationManager locationManager;
    double ax, ay, az;
    double latitude, longitude;
    private Camera mCamera;
    private CameraPreview mPreview;
    private OkHttpClient client;
    public File pictureFile;
    public ApplicationMy app;
    public String json;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    Timer timer, timerImg;
    TimerTask t, tImg;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cameraview);

        // Create an instance of Camera
        mCamera = getCameraInstance();

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);

        if (checkPermission()) {
            Log.v("Kamera","Permission is granted");
        }else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }

        app = (ApplicationMy) getApplication();
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(1, TimeUnit.MINUTES) // connect timeout
                .writeTimeout(1, TimeUnit.MINUTES) // write timeout
                .readTimeout(1, TimeUnit.MINUTES); // read timeout
        client = builder.build();
        startAcc();
        startLoc();
        timer = new Timer();
        t = new TimerTask() {
            @Override
            public void run() {
                getDataAcc();
            }
        };
        timer.scheduleAtFixedRate(t,50,50);
        timerImg = new Timer();
        tImg = new TimerTask() {
            @Override
            public void run() {
                zajemi();
            }
        };
        timerImg.scheduleAtFixedRate(tImg,5000,5000);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(CameraView.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    private void getDataAcc(){
        Log.d("DobiAcc","Smo v getDataAcc");
        AccelerometerData novi = new AccelerometerData();
        novi.setZ(ax);
        novi.setLatitude(latitude);
        novi.setLongitude(longitude);
        Log.d("Acc", "Longitude: " + longitude);
        Log.d("Acc", "Latitude: " + latitude);
        app.addAccelerometerData(novi);
        if(app.getAccelerometerData().size()>499){
            sendAccData();
        }
    }

    public void sendAccData() {
        ArrayList<AccelerometerData> podatki;
        podatki = app.getAccelerometerData();
        Gson gson = new Gson();
        json = gson.toJson(podatki);
        String url = "http://83.212.127.94:3000/api/rc/addNew";

        RequestBody body = RequestBody.create(
                json,
                MediaType.parse("application/json; charset=utf-8")
        );

        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e)
            {
                Log.d("Acc", "Napaka!");
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.d("Acc", response.body().string());
                    throw new IOException("Unexpected code " + response);
                } else {
                    Log.d("Acc", "Podatki poslani.");
                    app.clearAccelerometerData();
                }
            }
        });
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    public Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null){
                Log.d("Kamera", "Error creating media file, check storage permissions");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                mCamera.stopPreview();
                mCamera.startPreview();
                sendImgData();
            } catch (FileNotFoundException e) {
                Log.d("Kamera", "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d("Kamera", "Error accessing file: " + e.getMessage());
            }
        }
    };

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        //File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),"");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "RoadApp");
        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdir()){
                Log.d("MyCameraApp", "failed to create directory");
                //return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        int MEDIA_TYPE_VIDEO = 2;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void zajemi(){
        mCamera.takePicture(null, null, mPicture);
    }


    public void sendImgData() {
        String url = "http://83.212.127.94:3000/api/ts/addNew";
        MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("latitude", String.valueOf(latitude))
                .addFormDataPart("longitude", String.valueOf(longitude))
                .addFormDataPart("image", "image.jpg", RequestBody.create(MEDIA_TYPE_JPG, new File(pictureFile.getPath())))
                .build();

        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e)
            {
                Log.d("Acc", "Napaka!");
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.d("Acc", response.body().string());
                    throw new IOException("Unexpected code " + response);
                } else {
                    Log.d("Acc", "Podatki poslani.");
                    Log.d("Acc", response.body().string());
                }
            }
        });
    }

    public void end(View v){
        timer.cancel();
        timerImg.cancel();
        t.cancel();
        tImg.cancel();
        sensorManager.unregisterListener(this);
        locationManager.removeUpdates(this);
        finish();
    }

    private void startAcc() {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void startLoc() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            ax=event.values[0];
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        Log.d("LocTest latitude: ", latitude + "");
        Log.d("LocTest longitude: ", longitude + "");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }

}