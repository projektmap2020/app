package com.example.roadapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private ApplicationMy app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        app = (ApplicationMy) getApplication();
    }

    public void zacni(View e){
        Intent i = new Intent(getBaseContext(), CameraView.class);
        startActivity(i);
    }

    public void startCapture(View v){
        Intent i = new Intent(getApplicationContext(), CaptureActivity.class);
        startActivity(i);
    }
}
