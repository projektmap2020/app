package com.example.roadapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.libaccelerometerdata.AccelerometerData;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CaptureActivity extends AppCompatActivity implements SensorEventListener, LocationListener {
    private SensorManager sensorManager;
    private LocationManager locationManager;
    double ax, ay, az;
    double latitude, longitude;
    TextView accData;
    public ApplicationMy app;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private Bitmap mImageBitmap;
    private String mCurrentPhotoPath;
    private ImageView mImageView;
    String json;
    private OkHttpClient client;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);
        accData = findViewById(R.id.accData);
        app = (ApplicationMy) getApplication();
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(1, TimeUnit.MINUTES) // connect timeout
                .writeTimeout(1, TimeUnit.MINUTES) // write timeout
                .readTimeout(1, TimeUnit.MINUTES); // read timeout
        client = builder.build();
        startAcc();
        startLoc();
        Timer timer = new Timer();
        TimerTask t = new TimerTask() {
            @Override
            public void run() {
                getDataAcc();
            }
        };
        timer.scheduleAtFixedRate(t,50,50);
        Timer timerImg = new Timer();
        TimerTask tImg = new TimerTask() {
            @Override
            public void run() {
                getDataImg();
            }
        };
        timerImg.scheduleAtFixedRate(tImg,5000,5000);
    }

    private void getDataAcc(){
        Log.d("DobiAcc","Smo v getDataAcc");
        AccelerometerData novi = new AccelerometerData();
        novi.setZ(ax);
        novi.setLatitude(latitude);
        novi.setLongitude(longitude);
        Log.d("Acc", "Longitude: " + longitude);
        Log.d("Acc", "Latitude: " + latitude);
        app.addAccelerometerData(novi);
        if(app.getAccelerometerData().size()>499){
            sendAccData();
        }
    }

    public void sendAccData() {
        ArrayList<AccelerometerData> podatki;
        podatki = app.getAccelerometerData();
        Gson gson = new Gson();
        json = gson.toJson(podatki);
        String url = "http://83.212.127.94:3000/api/rc/addNew";

        RequestBody body = RequestBody.create(
                json,
                MediaType.parse("application/json; charset=utf-8")
        );

        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e)
            {
                Log.d("Acc", "Napaka!");
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.d("Acc", response.body().string());
                    throw new IOException("Unexpected code " + response);
                } else {
                    Log.d("Acc", "Podatki poslani.");
                    app.clearAccelerometerData();
                }
            }
        });
    }

    private void getDataImg(){

    }

    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    //if (cameraIntent.resolveActivity(getPackageManager()) != null) {
        // Create the File where the photo should go
    //    File photoFile = null;
    //    try {
    //        photoFile = createImageFile();
    //    } catch (IOException ex) {
            // Error occurred while creating the File
    //        Log.i(TAG, "IOException");
    //    }
        // Continue only if the File was successfully created
    //    if (photoFile != null) {
    //        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
    //        startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
    //    }
    //}

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    //@Override
    //protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    //    if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
    //        try {
    //            mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(mCurrentPhotoPath));
    //            mImageView.setImageBitmap(mImageBitmap);
    //        } catch (IOException e) {
    //            e.printStackTrace();
    //        }
    //    }
    //}

    //public void klici(){
    //   Intent i = new Intent(getApplicationContext(), MainActivity.class);
    //    startActivityForResult(i,200);
    //}

    private void startAcc() {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void startLoc() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            ax=event.values[0];
            //Log.d("AccTest x:", ax + "");
            accData.setText("Acc data: " + ax);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void koncaj(View v){
        sensorManager.unregisterListener(this);
        locationManager.removeUpdates(this);
        finish();
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        Log.d("LocTest latitude: ", latitude + "");
        Log.d("LocTest longitude: ", longitude + "");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }
}
