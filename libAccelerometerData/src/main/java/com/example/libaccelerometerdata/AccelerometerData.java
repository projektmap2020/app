package com.example.libaccelerometerdata;

import javax.xml.stream.Location;

public class AccelerometerData {
    double z;
    double latitude;
    double longitude;

    public double getZ() {
        return z;
    }

    public void setZ(double forceX) {
        this.z = forceX;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "AccelerometerData{" +
                "forceX=" + z +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
